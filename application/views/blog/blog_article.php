<?php if (isset($post) && is_array($post) && count($post) > 0) { ?>
    <?php $post = $post[0]; ?>
    <article  class=" col-lg-9  text-right article">

        <a href="<?php echo base_url() . 'blog/article/' . $post->postGuid ?>" class=" row">
            <h1 class=" col-lg-12 text-center article-header" > <?php echo $post->postTitle ?> </h1>
        </a>
        <div class="row">
            <img class=" col-lg-12 img-responsive img-thumbnail article-photo" alt=" <?php echo $post->postTitle ?>" src="<?php echo $photoPath . $post->mediaTheme ?>" width="1024px" height= "600px" />
        </div>
        <div class="row">
            <div class=" col-lg-12 text-justify article-content">
                <?php echo (empty($post->postContent) ? '' : trim($post->postContent)) ?>
            </div>
        </div>
        <div class=" row article-footer" >
            <div class="col-lg-3">
                <label>موضوع : </label>
                <?php echo (empty($post->termCaption) ? '' : trim($post->termCaption)) ?>
            </div>
            <div class="col-lg-3">

                <label>نویسنده : </label>
                <?php echo (empty($post->userEmail) ? '' : trim($post->userEmail)) ?>
            </div>
            <div class="col-lg-3">

                <label>زمان انتشار : </label>
                <?php echo (empty($post->postDate) ? '' : trim($post->postDate)) ?>
            </div>
        </div>



    </article> 
<?php } ?>
<nav  class=" col-lg-3 ">
    <?php if (isset($posts) && is_array($posts) && count($posts) > 0) { ?>
        <?php foreach ($posts as $side) { ?>
            <a href="<?php echo base_url() . 'blog/article/' . $side->postGuid ?>"  target="_blank"  class=" row  text-right sidepost">

                <h3 class="row  text-center sidepost-header" > <?php echo $side->postTitle ?> </h3>


                <img class=" row img-responsive img-thumbnail sidepost-photo" alt=" <?php echo $side->postTitle ?>" src="<?php echo $photoThemePath . $side->mediaTheme ?>" width="300px" height= "200px" />

                <div class=" row text-justify sidepost-content">
                    <?php echo (empty($side->postExcerpt) ? '' : substr(trim(strip_tags($side->postExcerpt)), 0, 100) . '...') ?>
                </div>


            </a >
            <?php
        }
    }
    ?>

</nav>
<div class="gap"></div>
<div class="col-lg-9">
    <form class="row"  role="form" method="post" accept-charset="utf-8" action="<?php echo base_url(); ?>admin/blog/addTerm">

        <div class="form-group">
            <input type="text" placeholder="نویسنده" class="form-control" name="termName"  value="<?php echo set_value('termName'); ?>"  />
        </div>
        <?php if (form_error('termName')) { ?>
            <div class="alert alert-danger"><?php echo form_error('termName') ?></div>
        <?php } ?>
        <div class="form-group">
            <input type="email" placeholder="پست الکترونیک" class="form-control" name="termCaption" value="<?php echo set_value('termCaption'); ?>"  />
        </div>
        <?php if (form_error('termCaption')) { ?>
            <div class="alert alert-danger"><?php echo form_error('termCaption') ?></div>
        <?php } ?> 
        <div class="form-group">
            <input  type="url" placeholder="ادرس صفحه" class="form-control" name="termCaption" value="<?php echo set_value('termCaption'); ?>"  />
        </div>
        <div class="">
            <textarea rows="5" placeholder="محتوی" class="form-control" name="termCaption" value="<?php echo set_value('termCaption'); ?>"  >

            </textarea>
        </div>
        <?php if (form_error('termCaption')) { ?>
            <div class="alert alert-danger"><?php echo form_error('termCaption') ?></div>
        <?php } ?>
        <button type="submit" class="btn  btn-primary col-lg-12"><i class="fa fa-paper-plane" ></i> افزودن</button>




    </form> 
    <section  class=" row">
        <div  class=" row  text-right sidepost">
          
            
            <h3 class="row  text-center sidepost-header" > <?php echo $side->postTitle ?> </h3>


            <img class=" row img-responsive img-thumbnail sidepost-photo" alt=" <?php echo $side->postTitle ?>" src="<?php echo $photoThemePath . $side->mediaTheme ?>" width="300px" height= "200px" />

            <div class=" row text-justify sidepost-content">
                <?php echo (empty($side->postExcerpt) ? '' : substr(trim(strip_tags($side->postExcerpt)), 0, 100) . '...') ?>
            </div>


        </div >

    </section>
</div>



