<?php $themePath= base_url().'themes/index/';?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $themePath; ?>img/phpLover.ico">

		<meta name="description" content="<?php echo (isset($description))?$description: ' شرکت طراخی و پیا ده سازی نرم افزار و میر بانی و پیا ذه سازی وب سایت های اختصاصی'?>" />
		<meta name="keywords" content="<?php echo (isset($keywords))? $keywords: 'طراحی شبکه, طراحی نرم افزار , شبکه'?>" />
		<meta name="language" content="en" />
	
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo (isset($title))? $title: 'PHP Lover';?></title>

        <!-- Bootstrap -->
        <link href="<?php echo $themePath; ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $themePath ;?>css/bootstrap-rtl.min.css" rel="stylesheet">

        <link href="<?php echo $themePath ;?>css/style.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <nav class="navbar  navbar-inverse navbar-fixed-top" id="top-nav">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header navbar-right" >
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand active " style="margin-left: 5px;" href="#header">PHPLover</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div id="navbar" class="collapse navbar-collapse" >
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#menu_tool">خدمات </a></li>
                        <li ><a href="#menu_learning">آموزش </a></li>
                        <li><a href="#menu_service">سرویس </a></li>
                        <li><a href="#menu_about">درباره </a></li>

                    </ul>
                    <form class="navbar-form navbar-left" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="جستجو">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">جستجو</button>
                            </span>
                        </div><!-- /input-group -->
                    </form>

                </div><!-- /.navbar-collapse -->
            </div>
        </nav>
        <div class="container header" id="header">
            <h1>PHP Lover</h1>
        </div>


        <div id="myCarousel" class="carousel slide" data-ride="carousel" > 
            <!-- Indicators -->

            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" >
                <div class="item active"> <img src="<?php echo $themePath ;?>img/slider/code.jpg" style="width:100%" alt="First slide">
                    <div class="container">
                        <div class="carousel-caption">
                        </div>
                    </div>
                </div>
                <div class="item"> <img src="<?php echo $themePath ;?>img/slider/security.jpg" style="width:100%" data-src="" alt="Second    slide">
                    <div class="container">
                        <div class="carousel-caption">
                            
                        </div>
                    </div>
                </div>
                <div class="item"> <img src="<?php echo $themePath ;?>img/slider/team.jpg" style="width:100%" data-src="" alt="Third slide">
                    <div class="container">
                        <div class="carousel-caption">
                           
                        </div>
                    </div>
                </div>
            </div>
            <a class="left carousel-control" href="#myCarousel" data-slide="prev"></a> <a class="right carousel-control" href="#myCarousel" data-slide="next"></a> </div>

        <div class="row gap" >
        </div>
        <div class="row" id="menu_tool">

            <div class="row sectionHeader" >
                
                <h1>خدمات</h1> 
            </div>
            <div class="container marketing">

                <!-- Three columns of text below the carousel -->
                <div class="row">
                    <div class="col-lg-4">
                        <img class="img-circle" src="<?php echo $themePath ;?>img/gallery/webdesign.gif" alt="طراحی سایت" style="width: 140px; height: 140px;">
                        <h2 >طراحی سایت</h2>
                        <p>طراحی سایت داینامیک به صورت اختصاصی و حرفه ای <br/>
طراحی سایت فروشگاهی با گرافیک بالا و سفارشی سازی<br/>
طراحی پرتال(پورتال)،سایت و وب سایت برای سازمان ها و شرکت ها</p>
                        
                    </div><!-- /.col-lg-4 -->
                    <div class="col-lg-4">
                        <img class="img-circle" src="<?php echo $themePath ;?>img/gallery/hosting.gif" alt="میزبانی وب" style="width: 300px; height: 300px;">
                        <h2>میزبانی وب</h2>
                        <p>
                            ما با در اختیار داشتن سرورهای متعدد با سیستم عامل های متفاوت و متنوع این امکان را در اختیار مشتریان خود قرار داده است تا مناسبترین سرور را برحسب نیاز و سلیقه ، جهت میزبانی وب سایت خود انتخاب نماید . 
                            
                        </p>
                        
                    </div>

                    <div class="col-lg-4">
                        <img class="img-circle" src="<?php echo $themePath ;?>img/gallery/desktopapps.gif" alt="نرم افزار" style="width: 140px; height: 140px;">
                        <h2>طراحی نرم افزار</h2>
                        <p>
                           گاهی بر اساس نیاز کسب و کار خود نیاز به نرم افزاری دارید که در بازار وجو ندارد و یا اگر وجود دارد قسمت هایی کم یا زیاد دارد. ساخت نرم افزار های اختصاصی کاملاً مرتبط بر نیاز و سخت افزار خود را به ما بسپارید
                        </p>
                       
                    </div>

                </div><!-- /.row -->
                <div class="row">
                    <div class="col-lg-6">
                        <img class="img-circle" src="<?php echo $themePath ;?>img/gallery/Learning.gif" alt="آموزش" style="width: 140px; height: 140px;">
                        <h2>آموزش</h2>
                        <p>
                            بخش آموزشگاه با هدف آموزش تخصصی و تربیت نیروی متخصص در علوم مختلف مرتبط به شبکه ،  برنامه نویسی و توسعه نرم افزار فعالیت می کند. اساتید این آموزشگاه همگی کسانی هستند که خود برنامه نویس هستند و تجربه طولانی مدتی در اجرای پروژه های نرم افزاری و شبکه  دارند.
                            
                        </p>
                        
                    </div><!-- /.col-lg-4 -->
                    <div class="col-lg-6">
                        <img class="img-circle" src="<?php echo $themePath ;?>img/gallery/network.jpg" alt="پیاده سازی شبکه" style="width: 140px; height: 140px;">
                        <h2>پیاده سازی شبکه</h2>
                        <p>
                            اگر به دنبال پیاده سازی استاندارد و پشتیبانی واقعی در طراحی شبکه می گردید ما با متخصصان حرفه ای خود در خدمت شماست.

                        </p>
                    </div>
                </div><!-- /.row -->
            </div>


            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="<?php echo $themePath ;?>js/jquery.min.js"></script>
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <script src="<?php echo $themePath ;?>js/bootstrap.min.js"></script>
            <script src="<?php echo $themePath ;?>js/script.js"></script>
    </body>
</html>