$(document).ready(function() {
    $('a[href^="#menu_"]').on('click', function(e) {
        e.preventDefault();

        var target = this.hash;
        $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': ($target.offset().top) - 30
        }, 1800, 'swing', function() {
            window.location.hash = target - 30;
        });
    });


    var nav = $('#popupMenu');
    $(window).scroll(function() {
        if ($(this).scrollTop() > 600) {
            nav.addClass("f-nav");
        } else {
            nav.removeClass("f-nav");
        }
    });

		var winsize = wndsize();
    $('#header').height(winsize.height-$('#header h1').height()/4);
    $('#myCarousel img').height(winsize.height);
    $('#header').css("padding-top", function(index) {
        var winHeight = winsize.height
        var fonth = $('#header h1').height();
        return ((winHeight / 2) - (fonth));
    });

    $(window).resize(function() {
		var winsize = wndsize();
        $('#header').height(winsize.height-$('#header h1').height()/4);
    $('#myCarousel img').height(winsize.height);
        $('#header').css("padding-top", function(index) {
            var winHeight = winsize.height;
            var fonth = $('#header h1').height();
            return ((winHeight / 2) - (fonth));
        });

    });
	
	

function wndsize(){
var w = 0;var h = 0;
//IE
if(!window.innerWidth){
    if(!(document.documentElement.clientWidth == 0)){
        //strict mode
        w = document.documentElement.clientWidth;h = document.documentElement.clientHeight;
    } else{
        //quirks mode
        w = document.body.clientWidth;h = document.body.clientHeight;
    }
} else {
    //w3c
    w = window.innerWidth;h = window.innerHeight;
}
return {width:w,height:h};
}
function wndcent(){
var hWnd = (arguments[0] != null) ? arguments[0] : {width:0,height:0};
var _x = 0;var _y = 0;var offsetX = 0;var offsetY = 0;
//IE
if(!window.pageYOffset){
//strict mode
if(!(document.documentElement.scrollTop == 0)){offsetY = document.documentElement.scrollTop;offsetX = document.documentElement.scrollLeft;}
//quirks mode
else{offsetY = document.body.scrollTop;offsetX = document.body.scrollLeft;}}
//w3c
else{offsetX = window.pageXOffset;offsetY = window.pageYOffset;}_x = ((wndsize().width-hWnd.width)/2)+offsetX;_y = ((wndsize().height-hWnd.height)/2)+offsetY;
return{x:_x,y:_y};
}

});


