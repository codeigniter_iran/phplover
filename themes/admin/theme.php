<!doctype html>
<html lang="en">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>themes/admin/images/phpLover.ico">
	<meta name="description" content="<?php echo (isset($description))?$description: ' PHPLover'?>" />
	<meta name="keywords" content="<?php echo (isset($keywords))? $keywords: ''?>" />
	<meta name="language" content="en" />
	<title><?php echo (isset($title))? $title: 'PHPLover'?></title>
	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/css/layout.css" media="screen" />
	
	<!--[if lt IE 9]>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/admin/css/ie.css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<script type="text/javascript" src="<?php echo base_url();?>themes/lib/js/jquery-1.5.2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>themes/lib/js/hideshow.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>themes/lib/js/jquery.tablesorter.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>themes/lib/js/jquery.equalHeight.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>themes/admin/js/script.js"></script>
	
	<?php if (isset($includes)){echo $includes;} ?>

</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title"><a href="<?php echo base_url();?>admin">مدیریت سایت</a></h1>
			<h2 class="section_title">مدیریت</h2><div class="btn_view_site"><a href="<?php echo base_url();?>"><strong>نمایش سایت</strong></a></div>
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
            <p><?php echo  $this->session->userdata('userEmail')?></a></p>
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="index.html">Website Admin</a> <div class="breadcrumb_divider"></div><a href="index.html">Website Admin</a><div class="breadcrumb_divider"></div> <a class="current">Dashboard</a></article>
		</div>
	</section><!-- end of secondary bar -->
	
	<aside id="sidebar" class="column">
		<form class="quick_search">
			<input type="text" value="جستجو سریع" onfocus="if(!this._haschanged){this.value=''};this._haschanged=true;">
		</form>
		<hr/>
		<h3>محتوا</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="<?php echo base_url();?>admin/blog/addpost">ایجاد مقاله</a></li>
			<li class="icn_edit_article"><a href="<?php echo base_url();?>admin/blog/index">ویرایش مقاله</a></li>
			<li class="icn_categories">   <a href="<?php echo base_url();?>admin/blog/indexterm">دسته بندیها</a></li>
			<li class="icn_tags"><a href="#">تگ ها</a></li>
		</ul>
		<h3>کاربران</h3>
		<ul class="toggle">
			<li class="icn_add_user"><a href="<?php echo base_url();?>admin/users/add">افزودن کاربر</a></li>
			<li class="icn_view_users"><a href="<?php echo base_url();?>admin/users/index">نمایش کاربران</a></li>
			<li class="icn_profile"><a href="<?php echo base_url();?>admin/users/edit/<?php echo  $this->session->userdata('userId')?>">ویرایش پروفایل</a></li>
		</ul>
		<h3>رسانه</h3>
		<ul class="toggle">
			<li class="icn_folder"><a href="#">مدیریت فایل ها</a></li>
			<li class="icn_photo"><a href="<?php echo base_url();?>admin/gallery/index">گالری</a></li>
			<li class="icn_audio"><a href="#">صدا</a></li>
			<li class="icn_video"><a href="#">ویدئو</a></li>
		</ul>
		<h3>مدیریت</h3>
		<ul class="toggle">
			<li class="icn_settings"><a href="<?php echo base_url();?>admin/settings/index">تنظیمات</a></li>
			<li class="icn_security"><a href="#">امنیت</a></li>
			<li class="icn_jump_back"><a href="<?php echo base_url();?>logout">خروج</a></li>
		</ul>
		
		<footer>
			<hr />
			<p><strong>Copyright &copy; 2011 Website Admin</strong></p>
			<p>Theme by <a href="https://en.gravatar.com/alidonyaie">Ali Donyaie</a></p>
		</footer>
	</aside><!-- end of sidebar -->
	
	<section id="main" class="column">
	
    	<?php echo $content; ?>
    	<h4 class="alert_info">بارگزاری در ({elapsed_time}) ثانیه</h4>
		<div class="spacer"></div>
		
	</section>


</body>

</html>