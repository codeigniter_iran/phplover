<?php $themePath = base_url() . 'themes/index/'; ?>
<!DOCTYPE html>
<html lang="fa-ir">
    <head>
        <meta http-equiv="content-type" content="text/html" charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $themePath; ?>img/phpLover.ico"/>

        <meta name="description" content="<?php echo (isset($description)) ? $description : ' شرکت الکترو رایانه قائم خزر طراحی و پیا ده سازی نرم افزار و میز بانی و پیاده سازی وب سایت های اختصاصی' ?>" />
        <meta name="keywords" content="<?php echo (isset($keywords)) ? $keywords : 'طراحی شبکه, طراحی نرم افزار ,سرویس , سرویس گرایی , soap , php lover , خدمات , میزبانی وب, آموزش  برنامه نویسی , شبکه' ?>" />


        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <title><?php echo (isset($title)) ? $title : 'PHP Lover : Home'; ?></title>

        <!-- Bootstrap -->
        <link href="<?php echo $themePath; ?>css/bootstrap.min.css" rel="stylesheet"/>
        <link href="<?php echo $themePath; ?>css/bootstrap-rtl.min.css" rel="stylesheet"/>

        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"/>

        <link href="<?php echo $themePath; ?>css/style.css" rel="stylesheet"/>
        <link rel="alternate" href="<?php echo base_url() ?>" hreflang="fa-ir" />


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container-fluid" id="container">



            <nav class="navbar  navbar-inverse navbar-fixed-top" id="top-nav">
                <div class="container-fluid">

                    <div  class="navbar-header navbar-right active" >
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="nav navbar-brand " style="margin-left: 5px;" href="#menu_header">PHPLover</a>
                    </div>

                    <div id="navbar" class="collapse navbar-collapse" >
                        <ul class="nav navbar-nav navbar-right">
                            <li ><a  href="#menu_tool">خدمات </a></li>
                            <li ><a href="#menu_training">مقالات </a></li>
                            <li><a href="#menu_service">سرویس </a></li>
                            <li><a href="#menu_team">درباره </a></li>

                            <?php
                            if ($this->user_model->Secure(array('userType' => array('admin', 'user')))) {
                                echo '<li><a href="' . base_url() . 'admin">مدیریت</a></li>';
                                echo '<li ><a href="' . base_url() . 'logout">خروج</a></li>';
                            } else
                                echo '<li><a href="' . base_url() . 'login">ورود</a></li>';
                            ?>

                        </ul>
                        <form class="navbar-form navbar-left" role="search"  method="post" accept-charset="utf-8" action="<?php echo base_url(); ?>blog/search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="searchQuery" value="<?php echo set_value('searchQuery'); ?>" placeholder="جستجو">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">جستجو</button>
                                </span>
                            </div>
                        </form>

                    </div>
                </div>
            </nav>

            <div class="container header" id="menu_header">
                <h1>PHP Lover</h1>
            </div>


            <div id="myCarousel" class="carousel slide" data-ride="carousel" > 
                <!-- Indicators -->

                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" >
                    <div class="item active"> <img src="<?php echo $themePath; ?>img/slider/code.jpg" style="width:100%" alt="خبره در برنامه نویسی" width="1024px" height= "600px" />
                        <div class="container">
                            <div class="carousel-caption">
                            </div>
                        </div>
                    </div>
                    <div class="item"> <img src="<?php echo $themePath; ?>img/slider/security.jpg" style="width:100%" data-src="" alt="متتخصص امنیت" width="1024px" height= "600px" />
                        <div class="container">
                            <div class="carousel-caption">

                            </div>
                        </div>
                    </div>
                    <div class="item"> <img src="<?php echo $themePath; ?>img/slider/team.jpg" style="width:100%" data-src="" alt="کار گروهی منسجم" width="1024px" height= "600px" />
                        <div class="container">
                            <div class="carousel-caption">

                            </div>
                        </div>
                    </div>
                </div>
                <a class="left carousel-control" href="#myCarousel" data-slide="prev"></a> <a class="right carousel-control" href="#myCarousel" data-slide="next"></a> </div>

            <div class="gap" ></div>
            <section class="row" id="menu_training">


                <div class="container">
                    <?php if (isset($learning)) echo $learning; ?>
                </div>
            </section>


            <div class="gap" ></div>

            <!-- The circle icons use Font Awesome's stacked icon classes. For more information, visit http://fontawesome.io/examples/ -->
            <section id="services" class="services">
                <div class="container">
                    <div class="row text-center">
                        <div class="col-lg-10 col-lg-offset-1">
                            <h2>خدمات ما</h2>
                            <hr class="small">
                            <div class="row">
                                <div class="col-md-3 col-sm-6">
                                    <div class="service-item">
                                        <span class="fa-stack fa-4x">
                                            <i class="fa fa-circle fa-stack-2x services-circle"></i>
                                            <i class="fa fa-leaf fa-stack-1x services-icon "></i>
                                        </span>
                                        <h4>
                                            <strong>طراحی سایت</strong>
                                        </h4>
                                        <p>سایت های سازمانی ، شخصی و تجاری خود را به ما بسپارید</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6">
                                    <div class="service-item">
                                        <span class="fa-stack fa-4x">
                                            <i class="fa fa-circle fa-stack-2x services-circle"></i>
                                            <i class="fa fa-compass fa-stack-1x services-icon"></i>
                                        </span>
                                        <h4>
                                            <strong>طراحی نرم افزار </strong>
                                        </h4>
                                        <p>طراحی نرم افزار هایی کارا ، با امنیت بالا و مناسب برای پلتفرم های نام آشنا</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6">
                                    <div class="service-item">
                                        <span class="fa-stack fa-4x">
                                            <i class="fa fa-circle fa-stack-2x services-circle"></i>
                                            <i class="fa fa-flask fa-stack-1x services-icon"></i>
                                        </span>
                                        <h4>
                                            <strong>آموزش</strong>
                                        </h4>
                                        <p>اموزش زبان های رتر برنامه نویسی و متدولوژی های نوین</p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6">
                                    <div class="service-item">
                                        <span class="fa-stack fa-4x">
                                            <i class="fa fa-circle fa-stack-2x services-circle"></i>
                                            <i class="fa fa-shield fa-stack-1x services-icon"></i>
                                        </span>
                                        <h4>
                                            <strong>میزبانی وب</strong>
                                        </h4>
                                        <p>میزبانی وبسایت های شما با سرور های قدرت مند ایرانی و خارجی</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.col-lg-10 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container -->
            </section>



            <div class="gap"></div>

            <section id="menu_team" class="bg-light-gray">
                <div class="container">
                    <h2>اعضای گروه</h2>
                    <hr class="small">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="team-member">
                                <img src="<?php echo $themePath; ?>img/gallery/donyaie.jpg" class="img-responsive img-circle" alt="علی دنیایی" width="200px" height= "200px" />
                                <h4>Donyaie</h4>
                                <p class="text-muted">Lead Developer</p>
                                <ul class="row social-buttons">
                                    <li><a href="#"><span class="fa fa-linkedin"></span></a>
                                    </li>
                                    <li><a href="#"><span class="fa fa-facebook"></span></a>
                                    </li>
                                    <li><a href="#"><span class="fa fa-twitter"></span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="team-member">
                                <img src="<?php echo $themePath; ?>img/gallery/alimohammadi.jpg" class="img-responsive img-circle" alt="حمزه علیمحمدی" width="200px" height= "200px" />
                                <h4>Alimohammadi</h4>
                                <p class="text-muted">Lead Developer</p>
                                <ul class="row social-buttons">
                                    <li><a href="#"><span class="fa fa-linkedin"></span></a>
                                    </li>
                                    <li><a href="#"><span class="fa fa-facebook"></span></a>
                                    </li>
                                    <li><a href="#"><span class="fa fa-twitter"></span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-4" >

                    <h4>مکان ما</h4>
                    <span class="address">
                        ایران - مازندران - نوشهر- میدان آزادی- پاساژ ریاحی - واحد 4
                    </span>
                    <h4> شماره تماس</h4>
                    <span class="text-left phone">
                        11-523-58873(98+)
                    </span>
                </div>
            </section>
            <div class="gap"></div>


            <footer class="row">
                <div class="col-md-4 text-center">

                </div>
                <div class="col-md-4">
                    <ul class="row social-buttons">
                        <li><a href="#"><span class="fa fa-linkedin"></span></a>
                        </li>
                        <li><a href="#"><span class="fa fa-facebook"></span></a>
                        </li>
                        <li><a href="#"><span class="fa fa-twitter"></span></a>
                        </li>
                        <li>
                            <a href="https://plus.google.com/113881068068380192798" rel="publisher">‪<span class="fa fa-google-plus"></span>‏</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">

                </div>
            </footer>
        </div>
        <script src="<?php echo $themePath; ?>js/jquery.js"></script>
        <script src="<?php echo $themePath; ?>js/modernizr.js" type="text/javascript"></script>
        <script src="<?php echo $themePath; ?>js/bootstrap.min.js"></script>
        <script src="<?php echo $themePath; ?>js/script.js"></script>

        <!--<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script  src="<?php echo $themePath; ?>js/gmaps.js"></script> -->

        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-60761777-1', 'auto');
            ga('send', 'pageview');

        </script>
    </body>
</html>
