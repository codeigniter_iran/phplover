<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="<?php echo (isset($description))?$description: ' Mainframe - The complete PHP framework'?>" />
	<meta name="keywords" content="<?php echo (isset($keywords))? $keywords: ''?>" />
	<meta name="language" content="en" />
	<title><?php echo (isset($title))? $title: 'Mainframe - The complete PHP framework'?></title>
	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/lib/css/mainframe-grid.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/lib/css/mainframe-main.css" media="screen" />
	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/lib/css/jquery-ui-1.8.20.custom.css" rel="stylesheet" />
	<script type="text/javascript" src="<?php echo base_url();?>themes/lib/js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>themes/lib/js/lib/jquery-ui-1.8.20.custom.min.js"></script>
	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/demo/css/styles.css?v=<?php echo $this->config->item('cjsuf');?>" media="screen" />
	<script type="text/javascript" src="<?php echo base_url();?>themes/demo/js/script.js"></script>
	
	<?php if (isset($includes)){echo $includes;} ?>
</head>
<body>
<div class="fixed12">
	<div class="grid_10">
		<?php echo $content; ?>
	</div>
	<div class="grid_2">
		<?php
		
		if($this->user_model->Secure(array('userType'=>array('admin','user'))))
			$nav['خروج']=base_url().'logout';
        else
			$nav['ورود']=base_url().'login';
			
		$nav['صفحه اصلی']=base_url().'main';
		
		if($this->user_model->Secure(array('userType'=>array('admin'))))
			$nav['مدیریت']=base_url().'admin';
		
		
		if(isset($nav) AND is_array($nav)){ 
			echo "<ul class='nav' id='menu'>\n";
			foreach ($nav as $caption=>$address) { 
				echo "<li>\n";
				if(is_array( $address)){
					
					echo "<a class='slide' >$caption</a>\n";
            		echo "<ul class=nav >\n";
            		
					 foreach ($address as $key=>$value) {
						echo "<li>\n";
						echo"<a href='$value' >$key</a>\n";	
						echo "</li>\n";	
					 } 
					 
					 echo "</ul>\n";
					 
				}else{
					
					echo"<a href='$address' >$caption</a>\n";
						
				} 
				echo "</li>\n";
			 }
			 echo "</ul>";
 		} 
 		?>
	</div>
</div>
</body></html>