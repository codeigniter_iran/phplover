<!doctype html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="<?php echo (isset($description))?$description: ' Mainframe - The complete PHP framework'?>" />
	<meta name="keywords" content="<?php echo (isset($keywords))? $keywords: ''?>" />
	<meta name="language" content="en" />
	
	<title><?php echo (isset($title))? $title: 'Mainframe - The complete PHP framework'?></title>
	
    <script src="<?php echo base_url();?>themes/sample/js/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>themes/sample/js/jquery.dropotron.min.js" type="text/javascript"></script>
   
    <script src="<?php echo base_url();?>themes/sample/js/script.js" type="text/javascript"></script>
    
    <link rel="stylesheet" href="<?php echo base_url();?>themes/sample/css/main.css" type="text/css" />
    <!--[if IE]>
    	<script src="<?php echo base_url();?>themes/sample/js/html5shiv.js" type="text/javascript"></script>
    <![endif]-->
    <!--[if lte IE 7]>
    	<script src="http://code.google.com/p/ie7.js" type="text/javascript"></script>
    <![endif]-->
    <!--[if lt IE 7]>
    	<link rel="stylesheet" media="all" href="<?php echo base_url();?>themes/sample/css/ie6.css" type="text/css" />
    <![endif]-->
	
	<?php if (isset($includes)){echo $includes;} ?>
</head>
<body id="index" class="home">
	<header id="banner" class="body">
    	<h1>
        	<a href="#">
            	عنوان سایت html5
                </br>
                <strong> html5 از سال  <ins>2009</ins> تا <del>2022</del></strong>
            </a>
        </h1>
        <nav>
		<?php
			$nav['صفحه اصلی']=base_url().'main';
		
			
		?>
        	<ul>
       			<li class="active"><a href="<?php echo base_url()?>main">صفحه اصلی</a></li> 
       			<li ><a href="#">مقالات</a></li> 
       			<li><a href="#">ارتباط با ما</a></li> 
                
				
				<?php if($this->user_model->Secure(array('userType'=>array('admin','user')))){
						echo '<li><a href="'.base_url().'admin">مدیریت</a></li>';
						echo '<li class="logout"><a href="'.base_url().'logout">خروج</a></li>';
					}else
						echo '<li class="login"><a href="'.base_url().'login">ورود</a></li>';
				?>
        	</ul>
            
            
        </nav>
    </header><!--/#banner-->
    <aside id="featured" class="body">
    	<article>
        	<figure>
            	<img src="<?php echo base_url();?>themes/sample/img/sm-logo.jpg" alt="Codeigniter Theme"/>
            </figure>
            <hgroup>
            	<h2>Featured Article</h2>
                <h3><a href="#">Codeigniter Html5 Theme </a></h3>
            </hgroup>
            <p>
            	DiScover how to use Graceful Degradation and P'rogressive Enhancement techniques to achieve an outstanding , cross-browser
                <a href="http://dev.w3.org/html5/spec/Overview.html" rel="external">HTML5</a>
                and <a href="http://www.w3.org/TR/css3-roadmap/" rel="external">CSS</a> website today!
            </p>
        </article>
    </aside><!--/featured-->
    <section id="content" class="body">
        <?php echo $content; ?>
    </section><!--/#content-->
    <section id="extras" class="body">
        <div class="social">
        	<h2>social</h2>
            <ul>
            	<li><a href="http://facebook.com/ali1101368" rel="me" >facebook</a></li>
            	<li><a href="http://last.fm/ali1101368" rel="me" >lastfm</a></li>
            	<li><a href="http://linkedin.com/ali1101368" rel="me" >linkedin</a></li>
                
            	<li><a href="http://twitter.com/ali1101368" rel="me" >twitter</a></li>
            	<li><a href="#/feed/" rel="me" >rss</a></li>
            </ul>
        </div><!--/.social-->
    	<div class="blogroll">
        	<h2>blogroll</h2>
            <ul>
            	<li><a href="#" rel="external">HTMLDoctor</a></li>
            	<li><a href="#" rel="external">HTMLDoctor</a></li>
            	<li><a href="#" rel="external">HTMLDoctor</a></li>
                
            	<li><a href="#" rel="external">HTMLDoctor</a></li>
            	<li><a href="#" rel="external">HTMLDoctor</a></li>
            	<li><a href="#" rel="external">HTMLDoctor</a></li>
                
            	<li><a href="#" rel="external">HTMLDoctor</a></li>
            	<li><a href="#" rel="external">HTMLDoctor</a></li>
            	<li><a href="#" rel="external">HTMLDoctor</a></li>
                
            	<li><a href="#" rel="external">HTMLDoctor</a></li>
            	<li><a href="#" rel="external">HTMLDoctor</a></li>
            	<li><a href="#" rel="external">HTMLDoctor</a></li>
                
            	<li><a href="#" rel="external">HTMLDoctor</a></li>
            	<li><a href="#" rel="external">HTMLDoctor</a></li>
            	<li><a href="#" rel="external">HTMLDoctor</a></li>
                
            	<li><a href="#" rel="external">HTMLDoctor</a></li>
            	<li><a href="#" rel="external">HTMLDoctor</a></li>
            	<li><a href="#" rel="external">HTMLDoctor</a></li>
            </ul>
        </div><!--/.blogroll-->
    </section><!--/#extras-->
    <footer id="contentinfo" class="body">
    	<address id="about" class="vcard body">
        	<span class="primary">
            	<strong><a href="#" class="fn url">Smashing Magazine</a></strong>
                <span class="role">amazing Magazine</span>
            </span>
            <img src="<?php echo base_url();?>themes/sample/img/sm-logo.jpg" alt="Codeigniter Theme logo" class="photo"/>
            <span class="bio">Feedback, bug reports, and comments are not only welcome, but strongly encouraged :)
            Feedback, bug reports, and comments are not only welcome Feedback, bug reports, and comments are not only welcome
            </span> 
        </address><!--/#about-->
        <p>2005-2014<a href="#">Smashing Magazine</a></p>
    </footer><!--/#contentinfo-->
</body>
</html>
